package in.octalian.ws.dao;

import in.octalian.ws.model.Customer;
import in.octalian.ws.model.Order;

public interface CustomerDao {

	public void saveCustomerDetails(Customer cus);
	
	public Customer getCustomerDetails(int custId);
	
	public Order getOrderDetails(int custId);
	
	public Boolean checkifCustomerExists(int id);
}
