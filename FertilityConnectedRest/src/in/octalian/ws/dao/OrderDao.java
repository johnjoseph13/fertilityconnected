package in.octalian.ws.dao;

import in.octalian.ws.model.Order;

public interface OrderDao {

	void saveOrder(Order order);
	
	Order getOrderDetails(int id);
	
	void deleteOrder(int id);
}
