package in.octalian.ws.resteasy.client;

import static in.octalian.ws.daoImpl.HibernateUtil.getSessionFactory;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import in.octalian.ws.model.Customer;
import in.octalian.ws.model.GenericResponse;
import in.octalian.ws.model.Order;

public class RestEasyTestClient {

	public static void main(String[] args) {
		/*System.out.println("starting");
		Session newSession=getSessionFactory().openSession();
		Customer cus = new Customer();
		cus.setId(52);cus.setName("joseph");cus.setSalary(1000);
		newSession.beginTransaction();
		newSession.persist(cus);
		//newSession.flush();
		newSession.getTransaction().commit();
		newSession.close();
		System.out.println("stop");*/
		
		ResteasyClient client = new ResteasyClientBuilder().build();
		//GET example
		/*ResteasyWebTarget getDummy = client.target("http://localhost:8080/RestEasy-Example/customer/13/getDummy");

		Response getDummyResponse = getDummy.request().get();

		String value = getDummyResponse.readEntity(String.class);
        System.out.println(value);
        getDummyResponse.close();  */
		
		//get single client
		ResteasyWebTarget getSingle = client.target("http://localhost:8080/RestEasy-Example/customer/110/get");
		Response aresponse = getSingle.request().get();
		System.out.println("get single clients "+aresponse.readEntity(String.class));
		//System.out.println("get single clients "+aresponse.readEntity(Order.class));
		System.out.println("HTTP Response Code:"+aresponse.getStatus());
		aresponse.close();
		
		//get order 
		System.out.println("get order");
		ResteasyWebTarget getSingleOrder = client.target("http://localhost:8080/RestEasy-Example/customer/customerid/110/getOrder");
		Response aresponseOrder = getSingleOrder.request().get();
		System.out.println("get single clients "+aresponseOrder.readEntity(String.class));
		//System.out.println("get single clients "+aresponse.readEntity(Order.class));
		System.out.println("HTTP Response Code :"+aresponseOrder.getStatus());
		aresponseOrder.close();
		
		
		
		//POST example
		/*ResteasyWebTarget add = client.target("http://localhost:8080/RestEasy-Example/customer/add");
		Customer cust = new Customer();
		Order order =new Order();
		order.setProductID(131);order.setQuantity(20);
		order.setCost(100);order.setShippingAddress("india");
		order.setTotalCost(2000);
		cust.setId(110);cust.setName("john");cust.setSalary(1000);
		cust.setOrder(order);
		
		System.out.println("output");
		Response addResponse = add.request().post(Entity.entity(cust, MediaType.APPLICATION_XML));
		System.out.println(addResponse.readEntity(GenericResponse.class));
		System.out.println("HTTP Response Code:"+addResponse.getStatus());
		addResponse.close();*/
		/*
		emp.setId(10);emp.setName("john");emp.setSalary(10000);
		addResponse = add.request().post(Entity.entity(emp, MediaType.APPLICATION_XML));
		System.out.println(addResponse.readEntity(GenericResponse.class));
		System.out.println("HTTP Response Code:"+addResponse.getStatus());
		addResponse.close();*/


	}

}
