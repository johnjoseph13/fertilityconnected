package in.octalian.ws.resteasy.app;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import in.octalian.ws.serviceProviders.CustomerServiceImpl;
import in.octalian.ws.serviceProviders.OrderServiceImpl;

public class MobileApplication extends Application {
	
	private Set<Object> singletons = new HashSet<Object>();

	public MobileApplication() {
		singletons.add(new CustomerServiceImpl());
		singletons.add(new OrderServiceImpl());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}

}
