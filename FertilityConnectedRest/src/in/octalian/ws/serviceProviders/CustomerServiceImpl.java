package in.octalian.ws.serviceProviders;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import in.octalian.ws.dao.CustomerDao;
import in.octalian.ws.daoImpl.CustomerDaoImpl;
import in.octalian.ws.daoImpl.HibernateUtil;
import in.octalian.ws.model.Customer;
import in.octalian.ws.model.GenericResponse;
import in.octalian.ws.model.Order;
import in.octalian.ws.service.CustomerService;

@Path("/customer")


public class CustomerServiceImpl implements CustomerService {

	//private static Map<Integer,Customer> cust = new HashMap<Integer,Customer>();
	CustomerDao customer= new CustomerDaoImpl();

	@Override
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response addCustomer(Customer c) {
		GenericResponse response = new GenericResponse();
		System.out.println("here in add customer");
		if(c.getId()== 0){
			response.setStatus(false);
			response.setMessage(" please provide and ID");
			response.setErrorCode("EC-01");
			//HibernateUtil.getSessionFactory().close();
			return Response.status(422).entity(response).build();
		}


		Boolean value=customer.checkifCustomerExists(c.getId());
		if(value==true) {
			response.setStatus(false);
			response.setMessage(" Customer Already Exists");
			response.setErrorCode("EC-02");
			HibernateUtil.getSessionFactory().close();
			return Response.status(422).entity(response).build();
		}

		//cust.put(c.getId(), c);

		response.setStatus(true);
		response.setMessage("Customer created successfully");
		System.out.println("values :== :"+c.getId()+" :: " + c.getName() + " ::: order"+c.getOrder());
		customer.saveCustomerDetails(c);
		HibernateUtil.getSessionFactory().close();
		return Response.ok(response).build();
	}


	@Override
	@GET
	@Path("/{id}/get")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response getCustomer(@PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		System.out.println("id to retrieve value "+id);
		if(id== 0){
			response.setStatus(false);
			response.setMessage(" please provide id");
			response.setErrorCode("EC-01");
			HibernateUtil.getSessionFactory().close();
			return Response.status(422).entity(response).build();
		}
		Boolean value=customer.checkifCustomerExists(id);
		System.out.println(">>>>>>>boolean"+value);
		if(value==false) {
			response.setStatus(false);
			response.setMessage(" No customer found with id = "+ id +", please provide customer");
			response.setErrorCode("EC-03");
			HibernateUtil.getSessionFactory().close();
			return Response.status(422).entity(response).build();
		}
		Customer c= customer.getCustomerDetails(id);
		System.out.println("value in Customer service :: "+ c +"  Order "+c.getOrder());
		HibernateUtil.getSessionFactory().close();
		return Response.ok().entity(c).build();

	}

	@PUT
	@Path("/{id}/update")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response updateCustomer(@PathParam ("id") int id) {
		return null;
	}


	@GET
	@Path("/customerid/{id}/getOrder")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response getOrder(@PathParam("id") int id) {
		GenericResponse response = new GenericResponse();
		if(id== 0){
			response.setStatus(false);
			response.setMessage(" please provide id");
			response.setErrorCode("EC-01");
			return Response.status(422).entity(response).build();
		}
		Boolean value=customer.checkifCustomerExists(id);
		System.out.println(">>>>>>>boolean"+value);
		if(value==false) {
			response.setStatus(false);
			response.setMessage(" no order for this customer with id = "+id);
			response.setErrorCode("EC-04");
			HibernateUtil.getSessionFactory().close();
			return Response.status(422).entity(response).build();
		}
		System.out.println(id);
		System.out.println("id to retrieve Order value "+id);
		Order order= customer.getOrderDetails(id);
		//Set<Order> o=c.getOrder();		
		System.out.println("value in Order service :: "+ order +"  Order ");
		HibernateUtil.getSessionFactory().close();
		return Response.ok().entity(order).build();

	}

}
