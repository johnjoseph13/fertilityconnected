package in.octalian.ws.serviceProviders;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import in.octalian.ws.model.Order;
import in.octalian.ws.service.OrderService;

@Path("/order")
public class OrderServiceImpl implements OrderService {

	@Override
	@GET
	@Path("/getString")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public String tempvalue() {
		// TODO Auto-generated method stub
		return "hello Order";
	}
	
	@GET
	@Path("/{id}/getDummy")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Order getDummyOrder(@PathParam("id") int id) {
		Order o = new Order();
		o.setOrderId(1);
		o.setQuantity(13);
		o.setCost(500);
		o.setTotalCost(3000);
		o.setShippingAddress("india");
		return o;
	}

	@Override
	public Response addOrder(Order e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response deleteOrder(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response getOrder(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Order[] getAllOrder() {
		// TODO Auto-generated method stub
		return null;
	}

}
