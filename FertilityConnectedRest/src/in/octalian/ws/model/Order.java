package in.octalian.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.Cascade;

//productID, quantity, costPerProduct, totalCost, shippingAddress

@XmlRootElement( name = "Order")
@XmlType(propOrder={"orderId", "category", "quantity", "cost", "totalCost", "shippingAddress", "status", "dateOfOrder"})
@Entity
@Table(name="ordrtable")
public class Order {
	@Id 
	@Column(name="orderId")
	private int orderID;
	private String category;
	private int quantity;
	private double cost;
	private double totalCost;
	private String shippingAddress;
	private String status;
	private String dateOfOrder;
	
	@ManyToOne
	@JoinColumn(name="custId")
	private Customer customer;
	
	public Order() {
		
	}
	
	public int getOrderId() {
		return orderID;
	}
	
	public void setOrderId(int orderID) {
		this.orderID = orderID;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public double getCost() {
		return cost;
	}
	
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public double getTotalCost() {
		return totalCost;
	}
	
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	
	public String getShippingAddress() {
		return shippingAddress;
	}
	
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	@XmlTransient
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDateOfOrder() {
		return dateOfOrder;
	}

	public void setDateOfOrder(String dateOfOrder) {
		this.dateOfOrder = dateOfOrder;
	}

	@Override
	public String toString() {
		return orderID + "::"+ category +"::" + quantity + "::" +
				cost + "::"+ totalCost + "::" + shippingAddress +
				"::"+ status +"::" +dateOfOrder;
	}
	
}
