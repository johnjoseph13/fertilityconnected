package in.octalian.ws.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.Cascade;

@XmlRootElement(name = "customer")
@XmlType(propOrder={"id", "name", "email", "address1", "address2", "country", "pincode", "dateAdded", "state", "city", "order"})
@Entity
@Table(name="custdetails")
public class Customer {
	@Id
	@Column(name="custId")
	private int id;	
	private String name;
	//private double salary;
	//email,address1,address2,country,state,pincode,dateAdded
	private String email;
	private String address1;
	private String address2;
	private String country;
	private String pincode;
	private String dateAdded;
	private String state;
	private String city;
	
	@OneToMany(fetch=FetchType.LAZY, targetEntity=Order.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "custId", referencedColumnName="custId")
	@XmlElement
	private Set<Order> order;
	

	public Customer() {
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/*public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	/*@XmlTransient
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}*/
	@XmlTransient
	public Set<Order> getOrder() {
		return order;
	}

	public void setOrder(Set<Order> order) {
		this.order = order;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return id + "::" + name + "::" + address1 +" :: "+address2+" :: "+country+" :: "+
							state+" :: "+pincode+" :: "+dateAdded;
	}

}
