package in.octalian.ws.daoImpl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import in.octalian.ws.dao.*;
import in.octalian.ws.model.Customer;
import in.octalian.ws.model.Order;

import static in.octalian.ws.daoImpl.HibernateUtil.*;

import java.util.List;

import javax.persistence.EntityManager;

@SuppressWarnings("deprecation")
public class CustomerDaoImpl implements CustomerDao{

	/*String hibernatePropsFilePath = "RestExample/RestEasy-Example/WebContent/WEB-INF/hibernate.cfg.xml";
	File hibernatePropsFile = new File(hibernatePropsFilePath);

	Configuration configuration = new Configuration().configure(hibernatePropsFile);
	SessionFactory ses = configuration.buildSessionFactory();*/
	@Override
	public void saveCustomerDetails(Customer cus) {
		System.out.println("over here");
		Session newSession=getSessionFactory().openSession();
		System.out.println("customer in dao "+cus);
		System.out.println("order in dao "+cus.getOrder());

		newSession.beginTransaction();
		newSession.save(cus);
		for(Order o:cus.getOrder()) {
			System.out.println("order in loop"+o +">>>>>productid"+o.getOrderId());

			newSession.save(o);
		}

		newSession.flush();
		newSession.getTransaction().commit();
		newSession.close();

	}
	public Customer getCustomerDetails(int custId) {
		// EntityManager em = entityManagerFactory.createEntityManager();
		Session newSession=getSessionFactory().openSession();
		newSession.beginTransaction();
		Customer c=newSession.get(Customer.class, custId);
		System.out.println("customer :: "+c);
		System.out.println("order :: "+c.getOrder());
		newSession.getTransaction().commit();
		newSession.close();
		return c;
	}
	public Order getOrderDetails(int custId) {
		// EntityManager em = entityManagerFactory.createEntityManager();
		Session newSession=getSessionFactory().openSession();
		newSession.beginTransaction();
		Criteria cri=newSession.createCriteria(Order.class)
				.createAlias("customer", "a")
				.add(Restrictions.eq("a.id", custId));
		List lOrder=cri.list();
		Order order=(Order)lOrder.get(0);
		//Order c=newSession.get(Order.class, custId);
		System.out.println("Order :: "+order);
		//System.out.println("order :: "+c.getOrder());
		newSession.getTransaction().commit();
		newSession.close();
		return order;
	}
	
	public Boolean checkifCustomerExists(int id) {
		System.out.println("check if customer exixts");
		Session newSession=getSessionFactory().openSession();
		newSession.beginTransaction();
		 Query query = newSession.             
				    createQuery("select 1 from Customer c where c.id = :id");
				        query.setInteger("id", id);
				        System.out.println("boolean value >>>>>>>"+query.uniqueResult());
				    return (query.uniqueResult() != null);		
	}

}
