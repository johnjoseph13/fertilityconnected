package in.octalian.ws.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import in.octalian.ws.model.Customer;
import in.octalian.ws.model.Order;

	public class HibernateUtil {

		private static  SessionFactory sessionFactory;
		//private static  Session session;
		public static void openSessionFactory()  {
			try {
				Configuration conf= new Configuration();
				conf.addAnnotatedClass(Customer.class);
				conf.addAnnotatedClass(Order.class);
				conf.configure();
			    ServiceRegistry sr = new StandardServiceRegistryBuilder().applySettings(conf.getProperties()).build();
			    sessionFactory = conf.buildSessionFactory(sr);
			   // session= getSessionFactory().openSession();
				

			} catch (Throwable ex) {
				System.err.println("Initial SessionFactory creation failed." + ex);
				throw new ExceptionInInitializerError(ex);
			}
		}

		public static SessionFactory getSessionFactory() {
			openSessionFactory();
			return sessionFactory;
		}
		/*public static Session getHibernateSession(){
			return session;
		}*/

		

	


}
