package in.octalian.ws.service;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import in.octalian.ws.model.Customer;

public interface CustomerService {


	public Response addCustomer(Customer e);

	public Response getCustomer(int id);
	
	public Response updateCustomer( int id);

}
