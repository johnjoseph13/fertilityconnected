package in.octalian.ws.service;

import javax.ws.rs.core.Response;

import in.octalian.ws.model.Order;

public interface OrderService {
	
	public String tempvalue();

	public Response addOrder(Order e);
	
	public Response deleteOrder(int id);
	
	public Response getOrder(int id);
	
	public Order[] getAllOrder();
	

}
